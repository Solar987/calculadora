package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Controlador implements Initializable {
	@FXML
	TextField numerouno;
	@FXML
	TextField numerodos;
	@FXML
	Label resultado;

	@FXML
	private void sumarAccion(ActionEvent accion) {
		try {
			int num1 = Integer.parseInt(numerouno.getText());
			int num2 = Integer.parseInt(numerodos.getText());
			int suma = num1 + num2;

			resultado.setText("Suma: "+ num1 + " + " + num2 + ": " + suma);
		} catch (NumberFormatException es) {
			resultado.setText("No puedes introducir letras.");
		}
	}

	@FXML
	private void restarAccion(ActionEvent accion) {
		try {
			int num1 = Integer.parseInt(numerouno.getText());
			int num2 = Integer.parseInt(numerodos.getText());
			int resta = num1 - num2;

			resultado.setText("Resta: "+ num1 + " - " + num2 + ": " + resta);
		} catch (NumberFormatException a) {
			resultado.setText("No puedes introducir letras.");
		}
	}

	@FXML
	private void dividirAccion(ActionEvent accion) {
		try {
			int num1 = Integer.parseInt(numerouno.getText());
			int num2 = Integer.parseInt(numerodos.getText());
			try {
				int division = num1 / num2;

				resultado.setText("Division: "+ num1 + " / " + num2 + ": " + division);
			} catch (ArithmeticException t) {
				resultado.setText("No puedes hacer eso.");
			}
		} catch (NumberFormatException p) {
			resultado.setText("No puedes introducir letras.");
		}
	}

	@FXML
	private void multiplicacionAccion(ActionEvent accion) {
		try {
			int num1 = Integer.parseInt(numerouno.getText());
			int num2 = Integer.parseInt(numerodos.getText());

			int multiplicacion = num1 * num2;

			resultado.setText("Multiplica: "+ num1 + " * " + num2 + ": " + multiplicacion);
		} catch (NumberFormatException a) {
			resultado.setText("No puedes introducir letras.");
		}
	}

	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}
}
